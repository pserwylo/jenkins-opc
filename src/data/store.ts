import { createBrowserHistory } from 'history'
import { applyMiddleware, compose, createStore } from 'redux'
import {routerMiddleware, RouterState} from 'connected-react-router'
import createRootReducer, { migrations } from './reducer'
import thunk from "redux-thunk";
import {ISettings} from "../pages/settings/types";

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import hardSet from 'redux-persist/lib/stateReconciler/hardSet'
import {IDashboardData} from "../pages/dashboard/reducer";
import createMigrate from "redux-persist/es/createMigrate";

export const history = createBrowserHistory({
    basename: window.location.hostname.endsWith('gitlab.io') ? '/jenkins-opc' : ''
});

export interface IStoreState {
    settings: ISettings;
    router: RouterState;
    dashboard: IDashboardData;
}

export default function configureStore() {

    const persistConfig = {
        key: 'root',
        storage,
        stateReconciler: hardSet,
        version: 0,
        migrate: createMigrate(migrations),
    };

    const persistedReducer = persistReducer<IStoreState>(persistConfig, createRootReducer(history));

    // @ts-ignore
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const store = createStore(
        persistedReducer,
        composeEnhancers(
            applyMiddleware(
                routerMiddleware(history), // for dispatching history actions
                thunk,
            ),
        ),
    );

    const persistor = persistStore(store);

    return { store, persistor };
}