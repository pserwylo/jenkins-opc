import {combineReducers, Reducer} from 'redux'
import { connectRouter } from 'connected-react-router'
import { History } from 'history'
import { reducer as settingsReducer } from "../pages/settings/reducer";
import { reducer as dashboardReducer } from "../pages/dashboard/reducer";
import { IStoreState } from "./store";

const createRootReducer = (history:History):Reducer<IStoreState> => combineReducers({
    router: connectRouter(history),
    settings: settingsReducer,
    dashboard: dashboardReducer,
});

// Explicitly loose with types because the state will change over time and I don't want to have to maintain a type
// definition for every single version.
export const migrations:any = {
    0: (state:any) => ({
        ...state,
        settings: {
            ...state.settings,
            jenkins: {
                ...state.settings.jenkins,
                availableProjects: [],
            }
        }
    })
};

export default createRootReducer;