import {JenkinsStatus} from "../connectors/jenkins";

export interface IProjectStatus {
    project: string;
    status: JenkinsStatus;
}

export interface IProjectsInfo {
    projects: IProjectStatus[]
}
