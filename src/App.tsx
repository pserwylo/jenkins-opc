import React from 'react';
import configureStore, {history} from "./data/store";

import {Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router'
import AppWrapper from "./AppWrapper";
import {PersistGate} from "redux-persist/integration/react";
import { DndProvider } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'


/**
 * Sets up all relevant higher order components for the app to work:
 *  - Redux provider.
 *  - Local storage persistor for redux.
 *  - redux-router connector.
 */
const App = () => {

    const { store, persistor } = configureStore();

    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <DndProvider backend={HTML5Backend}>
                    <ConnectedRouter history={history}>
                        <AppWrapper />
                    </ConnectedRouter>
                </DndProvider>
            </PersistGate>
        </Provider>
    );

};

export default App;
