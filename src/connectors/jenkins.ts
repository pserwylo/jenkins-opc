import {IJenkinsProject} from "../pages/settings/types";

export interface IJenkinsServer {
    url: string;
    username: string;
    apiKey: string;
}

type JenkinsBuildResult = 'SUCCESS' | 'FAILURE';

const jenkinsJobFolderClass = 'com.cloudbees.hudson.plugins.folder.Folder';

export type JenkinsStatus = JenkinsBuildResult | 'RUNNING' | 'UNKNOWN';

interface IJenkinsProjectListResponse{
    jobs: {
        _class: string,
        name: string,
        url: string,
    }[]
}

interface IJenkinsProjectResponse {
    lastBuild: null | {
        number: number,
    }
}

interface IJenkinsBuildResponse {
    result: JenkinsBuildResult;
    building: boolean;
}

const get = async <T> (server:IJenkinsServer, path:string): Promise<T> => {
    const fetchUrl = `${server.url}/${path}/api/json`;
    const result = await fetch(fetchUrl, {
        headers: {
            Authorization: 'Basic ' + btoa( server.username + ":" + server.apiKey),
        }
    });

    return await result.json() as T;
};

const fetchProjectList = async (server:IJenkinsServer, dir:string|null = null): Promise<IJenkinsProjectListResponse> => {
    return await get<IJenkinsProjectListResponse>(server, dir == null ? '' : dir);
};

const fetchProject = (server:IJenkinsServer, projectPath:string): Promise<IJenkinsProjectResponse> => {
    return get<IJenkinsProjectResponse>(server, projectPath);
};

const fetchBuild = (server:IJenkinsServer, projectPath:string, buildNumber:number): Promise<IJenkinsBuildResponse> => {
    return get<IJenkinsBuildResponse>(server, `${projectPath}/${buildNumber}`);
};

export const getJenkinsProjectList = async (server:IJenkinsServer): Promise<IJenkinsProject[]> => {
    const projects:IJenkinsProject[] = [];

    const recurseIntoProjectList = async (path:string|null) => {
        try {
            const details = await fetchProjectList(server, path);
            for (const job of details.jobs) {
                if (job._class === jenkinsJobFolderClass) {
                    await recurseIntoProjectList(job.url.substring(server.url.length));
                } else {
                    projects.push({
                        name: job.name,
                        path: job.url.substring(server.url.length)
                    });
                }
            }
        } catch (error) {
            console.error("Error fetching project list from Jenkins (" + server.url + ").", error);
        }
    };

    await recurseIntoProjectList(null);
    return projects;
};

export const getJenkinsProjectStatus = async (server:IJenkinsServer, projectPath:string): Promise<JenkinsStatus> => {

    try {
        const project = await fetchProject(server, projectPath);

        if (project.lastBuild == null) {
            return 'UNKNOWN';
        }

        const buildData = await fetchBuild(server, projectPath, project.lastBuild.number);

        if (buildData.building) {
            return 'RUNNING';
        }

        if (buildData.result === 'SUCCESS') {
            return 'SUCCESS';
        } else if (buildData.result === 'FAILURE') {
            return 'FAILURE';
        }
    } catch (e) {
        console.error("Error fetching project details from Jenkins (" + server.url + ") for " + projectPath, e);
    }

    return 'UNKNOWN';
};
