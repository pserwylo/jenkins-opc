import React, {useEffect} from "react";
import {connect} from "react-redux";
import {IStoreState} from "../../data/store";
import {IJenkinsSettings} from "../../pages/settings/types";
import {checkProjects as checkProjectAction, SettingsActionTypes} from "../../pages/settings/reducer";
import {ThunkDispatch} from "redux-thunk";

interface IDataProps {
    settings: IJenkinsSettings;
}

interface ICallbackProps {
    checkProjects: (settings:IJenkinsSettings, projects:string[]) => any;
}

type IProps = IDataProps & ICallbackProps;

const _JenkinsStatusChecker = ({settings, checkProjects}:IProps) => {

    useEffect(() => {
        const callback = () => {
            checkProjects(settings, settings.projects);
        };

        callback();
        const interval = setInterval(callback, settings.refreshRateInSeconds * 1000);
        return () => clearInterval(interval);
    }, [settings, checkProjects]);

    return <div className="jenkins-status-checker" />;
};

const mapState = (state:IStoreState) => ({
    settings: state.settings.jenkins,
});

const mapDispatch = (dispatch:ThunkDispatch<IStoreState, any, SettingsActionTypes>) => ({
    checkProjects: (settings:IJenkinsSettings, projects:string[]) => dispatch(checkProjectAction(settings, projects)),
});

/**
 * While mounted, periodically pings the jenkins server for each project in the store to ascertain its latest build status.
 * Doesn't have any visual implementation.
 */
export const JenkinsStatusChecker = connect(mapState, mapDispatch)(_JenkinsStatusChecker);