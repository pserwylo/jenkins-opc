import {makeStyles} from "@material-ui/core";
import React, {useEffect, useState} from "react";
import clsx from "clsx";

const ledRadius = 2;
const ledMargin = 2;
export const ledWidth = ledRadius * 2 + ledMargin;

const useStyles = makeStyles(theme => ({
    led: {
        display: "inline-block",
        width: `${ledRadius * 2}px`,
        height: `${ledRadius * 2}px`,
        borderRadius: `${ledRadius}px`,
        marginRight: `${ledMargin}px`,
    },
    ledWrapper: {
        whiteSpace: 'nowrap',
        display: 'inline-block',
    },
}));

export type RGBValue = [number, number, number];
export type RGBAnimator = (timeBetweenZeroAndOne:number) => RGBValue;
export type RGB = RGBValue | RGBAnimator;

const getRGBValue = (colourSource:RGB, time:number = 0) => {
    return colourSource instanceof Array /* i.e. an RGBValue */
        ? colourSource
        : colourSource(time)
};

export const singleColour = (colour:RGB, numLeds:number):RGB[] => Array.from(Array(numLeds).keys()).map(i => colour);
export const repeatingColours = (colours:RGB[], numLeds:number) => Array.from(Array(numLeds).keys()).map(i => colours[i % colours.length]);

interface IProps {
    colours: RGB[];
    numGroups?: number;
    fadeCandyUrl?: string;
}

// Refresh (and recalculate animations) this often. Higher obviously means more CPU cycles.
const ledRefreshRate = 25; // ms
const browserRefreshRate = 250; // ms

// Divide all colours by this amount to make them less offensively bright... Leave at 1 for full brightness.
const colourDamping = 2;

// Don't recreate this every few milliseconds, because we are overwriting each element in the array every loop anyway.
const packet = new Uint8ClampedArray(4 + 512 * 3);

const hasAnimated = (colours:RGB[]) => {
    // If any of the colours are an array, then that means they must be an RGBAnimator.
    return colours.find((c:RGB) => !(c instanceof Array)) != null;
};

export const LedStrip = ({colours, numGroups, fadeCandyUrl}:IProps) => {

    const classes = useStyles();

    // Used for animating LEDs. Time will be a value between 0 and 1. If the RGB value for an LED is a RGBAnimator then
    // this value will be passed to the function in order to get an RGBValue to render.
    const [time, setTime] = useState(0);

    const [socket, setSocket] = useState<WebSocket|null>(null);

    useEffect(() => {
        // Check if there are any animated LEDs for display. If so, we'll setup a timer for them.
        // If not, we wont bother, because it becomes expensive to run them in the browser.
        if (!hasAnimated(colours)) {
            return;
        }

        const interval = setInterval(() => setTime(t => (t + 0.1) % 1), browserRefreshRate);
        return () => clearInterval(interval);

    }, [colours]);

    useEffect(() => {
        const writeFrame = (time:number) => {
            if (socket === null || socket.readyState !== 1 /* OPEN */) {
                return;
            }

            if (socket.bufferedAmount > packet.length) {
                // The network is lagging, and we still haven't sent the previous frame.
                // Don't flood the network, it will just make us laggy.
                // If fcserver is running on the same computer, it should always be able
                // to keep up with the frames we send, so we shouldn't reach this point.
                return;
            }

            // Dest position in our packet. Start right after the header.
            let dest = 4;

            for (let led = 0; led < colours.length; led++) {
                const rgbSource: RGB = colours[led];
                const rgb: RGBValue = getRGBValue(rgbSource, time);
                packet[dest++] = rgb[0] / colourDamping;
                packet[dest++] = rgb[1] / colourDamping;
                packet[dest++] = rgb[2] / colourDamping;
            }

            for (let unusedLed = colours.length; unusedLed < 512; unusedLed++) {
                packet[dest++] = 0;
                packet[dest++] = 0;
                packet[dest++] = 0;
            }

            socket.send(packet.buffer);
        };

        writeFrame(0);

        let time = 0;
        const interval = setInterval(() => {
            time = (time + 0.01) % 1;
            writeFrame(time);
        }, hasAnimated(colours) ? ledRefreshRate : 1000);
        return () => clearInterval(interval);

    }, [socket, colours]);

    // If the fadeCandyUrl changes, then we will rerender, cancel the previous effect (closing the previous socket),
    // and then create a new socket and store it in our state.
    useEffect(() => {
        if (fadeCandyUrl != null) {
            const socket = new WebSocket(fadeCandyUrl);
            setSocket(socket);
            return () => socket.close()
        }
    }, [fadeCandyUrl]);

    return (
        <div className={classes.ledWrapper}>
            {colours.map((colour, i) => {
                const rgb = getRGBValue(colour, time);
                return (
                    <Led
                        key={i}
                        r={rgb[0]} g={rgb[1]} b={rgb[2]}
                        group={numGroups == null ? undefined : Math.floor((i / colours.length) * numGroups)}
                    />
                );
            })}
        </div>
    );
};

interface ILedProps {
    r: number;
    g: number;
    b: number;
    group?: number;
}

const Led = React.memo(({r, g, b, group}: ILedProps) => {
    const classes = useStyles();

    const cssColour = `rgb(${r}, ${g}, ${b})`;

    const style:React.CSSProperties = {
        boxShadow: `${cssColour} 0 0 ${ledRadius * 1.5}px`,
        backgroundColor: cssColour,
    };

    return (
        <span
            className={clsx(classes.led, group == null ? null : `group-${group}`)}
            style={style}
        />
    );
});