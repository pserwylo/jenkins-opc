import React from "react";
import {LedStrip, RGB, RGBValue, singleColour} from "./LedStrip";
import {IProjectStatus} from "../data/types";
import {easing, ledRangeForProject} from "../utils";
import {JenkinsStatus} from "../connectors/jenkins";
import {connect} from "react-redux";
import {IStoreState} from "../data/store";

export type StatusAnimation = 'none' | 'flash-slow' | 'flash' | 'flash-fast';
export type StatusColourMap = { [key in JenkinsStatus]: RGBValue; };
export type StatusAnimationMap = { [key in JenkinsStatus]: StatusAnimation; };

export const maybeAnimateRgb = (rgb:RGBValue, animation: StatusAnimation): RGB => {
    switch(animation) {
        case "flash-slow": return flash(rgb, easing.easeInOutCubic);
        case "flash":      return flash(rgb, fast(easing.easeInOutCubic));
        case "flash-fast": return flash(rgb, faster(easing.easeInOutCubic));
        case "none":
        default:
            return rgb;
    }
};

export type Easer = (time:number) => number;

export const flash = (rgb:RGBValue, easer:Easer) => (time:number):RGBValue => {
    const newTime = time < 0.5 ? easer(time * 2) : easer(1 -(time - 0.5) * 2);
    return [rgb[0] * newTime, rgb[1] * newTime, rgb[2] * newTime];
};

export const fast = (easer:Easer) => (time:number) => easer((time * 2) % 1);
export const faster = (easer:Easer) => (time:number) => easer((time * 4) % 1);

interface IProps {
    numLeds: number;
    useAllLeds: boolean;
    ledsPerProject: number;
    projects: string[];
    projectStatuses: IProjectStatus[];
    fadeCandyUrl: string;
    statusColours: StatusColourMap,
    statusAnimations: StatusAnimationMap,
}

export const _JenkinsLedStrip = (props:IProps) => {

    const { projects, projectStatuses, numLeds, useAllLeds, ledsPerProject, fadeCandyUrl, statusColours, statusAnimations } = props;

    const colours:RGB[] = projects.reduce(
        (colours:RGB[], project:string) => {
            const ledRange = ledRangeForProject(project, projects, numLeds, useAllLeds, ledsPerProject);
            const numLedsForProject = ledRange[1] - ledRange[0];
            const projectStatus = projectStatuses.find(p => p.project === project);
            const status:JenkinsStatus = projectStatus == null ? 'UNKNOWN' : projectStatus.status;
            const colour = maybeAnimateRgb(statusColours[status], statusAnimations[status]);
            return colours.concat(singleColour(colour, numLedsForProject));
        },
        []
    );

    return <LedStrip colours={colours} fadeCandyUrl={fadeCandyUrl} />;
};

const mapState = (state:IStoreState):IProps => ({
    projects: state.settings.jenkins.projects,
    numLeds: state.settings.fadeCandy.numLeds,
    useAllLeds: state.settings.fadeCandy.useAllLeds,
    ledsPerProject: state.settings.fadeCandy.ledsPerProject,
    projectStatuses: state.settings.projects.statuses,
    statusColours:state.settings.fadeCandy.statusColours,
    statusAnimations: state.settings.fadeCandy.statusAnimations,
    fadeCandyUrl: `ws://${state.settings.fadeCandy.host}:${state.settings.fadeCandy.port}`,
});

/**
 * Wraps an LedStrip and tells it which colours to render based on the project statuses.
 * It will allocate as many LEDs as available to each project.
 */
export const JenkinsLedStrip = connect(mapState)(_JenkinsLedStrip);
