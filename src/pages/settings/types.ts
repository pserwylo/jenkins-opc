import {StatusAnimationMap, StatusColourMap} from "../../components/JenkinsLedStrip";
import {JenkinsStatus} from "../../connectors/jenkins";
import {IProjectStatus} from "../../data/types";

export interface IJenkinsProject {
    name: string;
    path: string;
}

export interface IJenkinsSettings {
    url: string; // don't include a trailing slash...
    username: string;
    apiKey: string;
    refreshRateInSeconds: number;
    availableProjects: IJenkinsProject[];

    // Paths to jenkins project (e.g. "/job/name-of-project").
    // Can include subdirectories (e.g. "/job/name-of-folder/job/name-of-project").
    // Each entry should correspond to the path property of one of the availableProjects.
    projects: string[];
}

export interface IFadeCandySettings {
    host: string;
    port: number;
    numLeds: number;
    useAllLeds: boolean;
    ledsPerProject: number;
    statusColours: StatusColourMap;
    statusAnimations: StatusAnimationMap;
}

export interface IProjectSettings {
    statuses: IProjectStatus[];
}

export interface ISettings {
    jenkins: IJenkinsSettings;
    fadeCandy: IFadeCandySettings;
    projects: IProjectSettings;
}