import {Dispatch, Reducer} from 'redux'
import { ISettings } from "./types";
import {getJenkinsProjectList, getJenkinsProjectStatus, IJenkinsServer} from "../../connectors/jenkins";
import {IStoreState} from "../../data/store";
import {IProjectStatus} from "../../data/types";

export const UPDATE_PROJECT_STATUSES = 'UPDATE_PROJECT_STATUSES';
export const UPDATE_SETTINGS = 'UPDATE_SETTINGS';

interface IUpdateProjectStatusAction {
    type: typeof UPDATE_PROJECT_STATUSES;
    statuses: IProjectStatus[];
}

interface IUpdateSettingsAction {
    type: typeof UPDATE_SETTINGS;
    settings: ISettings;
}

export type SettingsActionTypes = IUpdateSettingsAction | IUpdateProjectStatusAction;

export const updateSettings = (settings:ISettings):SettingsActionTypes => ({
    type: UPDATE_SETTINGS,
    settings,
});

export const saveSettings = (settings:ISettings) => async (dispatch:Dispatch, getState:() => IStoreState) => {
    const oldJ = getState().settings.jenkins;
    const newJ = settings.jenkins;
    if (oldJ.url !== newJ.url || oldJ.username !== newJ.username || oldJ.apiKey !== newJ.apiKey) {
        settings.jenkins.availableProjects = await getJenkinsProjectList(settings.jenkins);
    }

    dispatch(updateSettings(settings));
};

export const checkProjects = (server:IJenkinsServer, projects:string[]) => async (dispatch:Dispatch) => {
    const statuses:IProjectStatus[] = [];

    for (const project of projects) {
        const status = await getJenkinsProjectStatus(server, project);
        statuses.push({project, status});
    }

    dispatch({
        type: UPDATE_PROJECT_STATUSES,
        statuses,
    });
};

const initialSettings:ISettings = {
    jenkins: {
        url: "http://localhost:8080",
        username: "",
        apiKey: "",
        refreshRateInSeconds: 30,
        availableProjects: [],
        projects: [],
    },
    fadeCandy: {
        // Default host and port for the fadecandy server.
        host: "localhost",
        port: 7890,
        numLeds: 30,
        useAllLeds: true,
        ledsPerProject: 1,
        statusColours: {
            FAILURE: [255, 0, 0],
            RUNNING: [0, 0, 255],
            SUCCESS: [0, 255, 0],
            UNKNOWN: [50, 50, 50],
        },
        statusAnimations: {
            FAILURE: 'none',
            RUNNING: 'flash-fast',
            SUCCESS: 'none',
            UNKNOWN: 'none',
        }
    },
    projects: {
        statuses: [],
    }
};

export const reducer:Reducer<ISettings, SettingsActionTypes> = (state = initialSettings, action:SettingsActionTypes) => {
    switch (action.type) {
        case "UPDATE_SETTINGS":
            return action.settings;

        case UPDATE_PROJECT_STATUSES:
            const statuses = action.statuses;

            // If nothing has changed, then don't bother mutating state and causing a rerender.
            const different = statuses.find(newStatus => {
                const existingStatus = state.projects.statuses.find(p => p.project === newStatus.project);
                return existingStatus == null || existingStatus.status !== newStatus.status;
            });

            if (different == null) {
                return state;
            }

            return {
                ...state,
                projects: {
                    ...state.projects,
                    statuses: statuses
                }
            };

        default:
            return state;
    }
};
