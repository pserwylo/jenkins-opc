import {
    Button, Checkbox, FormControl, FormControlLabel, FormHelperText,
    Grid, List,
    ListItem, ListItemIcon,
    ListItemSecondaryAction,
    ListItemText, MenuItem,
    Paper, Select,
    TextField,
    Typography
} from "@material-ui/core";
import {makeStyles, rgbToHex} from '@material-ui/core/styles';
// @ts-ignore
import ColorPicker from 'material-ui-color-picker';
import React, {useCallback, useRef, useState} from "react";
import {IFadeCandySettings, IJenkinsProject, IJenkinsSettings, ISettings} from "./types";
import {connect} from "react-redux";
import {IStoreState} from "../../data/store";
import {saveSettings, SettingsActionTypes} from "./reducer";
import {DragableTypes, ledRangeForProject, PixelRange} from "../../utils";
import {JenkinsStatus} from "../../connectors/jenkins";
import {LedStrip, RGBValue} from "../../components/LedStrip";
import {maybeAnimateRgb, StatusAnimation, StatusAnimationMap, StatusColourMap} from "../../components/JenkinsLedStrip";
import {ThunkDispatch} from "redux-thunk";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";
import {DropTargetMonitor, useDrag, useDrop, XYCoord} from "react-dnd";
import {IProjectStatus} from "../../data/types";

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(3, 2),
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
}));

interface IDataProps {
    settings: ISettings;
}

interface ICallbackProps {
    onSubmit: (settings: ISettings) => void;
}

type IProps = IDataProps & ICallbackProps;

const jenkinsStatuses: JenkinsStatus[] = [
    'SUCCESS',
    'FAILURE',
    'RUNNING',
    'UNKNOWN',
];

const properCase = (text: string) => text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
export const projectLabel = (projectPath: string) =>
    projectPath.substring(projectPath.lastIndexOf("/") + 1)
        .replace("-", " ")
        .split(" ")
        .map(properCase)
        .join(" ");
const statusLabel = (status: JenkinsStatus) => properCase(status);
const toHex = (rgb: RGBValue) => rgbToHex(`rgb(${rgb[0]}, ${rgb[1]}, ${rgb[2]})`);
const fromHex = (hex: string): RGBValue => [
    parseInt(hex.substring(1, 3), 16),
    parseInt(hex.substring(3, 5), 16),
    parseInt(hex.substring(5, 7), 16),
];

const _Settings = (props: IProps) => {

    const [jenkins, setJenkinsData] = useState(props.settings.jenkins);
    const [fadeCandy, setFadeCandyData] = useState(props.settings.fadeCandy);

    const classes = useStyles();

    const sortProjects = (projects:string[], availableProjects:IJenkinsProject[]) => {
        const calcIndex = (path:string) => availableProjects.findIndex(p => p.path === path);
        return projects.sort((path1, path2) => calcIndex(path1) - calcIndex(path2))
    };

    const selectProject = (path:string, isSelected: boolean) => {
        const projects = isSelected
            ? sortProjects(jenkins.projects.concat(path), jenkins.availableProjects)
            : jenkins.projects.filter(p => p !== path);

        setJenkinsData({
            ...jenkins,
            projects
        });
    };

    const updateProjectOrder = (availableProjects:IJenkinsProject[]) => {
        setJenkinsData({
            ...jenkins,
            availableProjects,
            projects: sortProjects(jenkins.projects, availableProjects),
        });
    };

    return (
        <Grid container spacing={3}>
            <Paper className={classes.paper}>
                <Grid container spacing={3}>
                    <Grid item xs={12} md={8}>

                        <Grid container spacing={3}>

                            <Grid item xs={12}>
                                <Typography variant="h5">Jenkins Settings</Typography>
                            </Grid>

                            <Grid item xs={12}>
                                <Grid container spacing={3}>
                                    <Grid item xs={12} md={8}>
                                        <TextField
                                            label="URL"
                                            value={jenkins.url}
                                            onChange={event => setJenkinsData({...jenkins, url: event.target.value})}
                                            type="url"
                                            name="jenkins-url"
                                            variant="outlined"
                                            fullWidth
                                        />
                                    </Grid>

                                    <Grid item xs={12} md={4}>
                                        <TextField
                                            label="Refresh Rate (seconds)"
                                            value={jenkins.refreshRateInSeconds}
                                            onChange={event => setJenkinsData({
                                                ...jenkins,
                                                refreshRateInSeconds: parseInt(event.target.value)
                                            })}
                                            type="number"
                                            name="jenkins-refresh-rate-in-seconds"
                                            variant="outlined"
                                            fullWidth
                                        />
                                    </Grid>

                                    <Grid item xs={12} md={6}>
                                        <TextField
                                            label="Username"
                                            value={jenkins.username}
                                            onChange={event => setJenkinsData({
                                                ...jenkins,
                                                username: event.target.value
                                            })}
                                            name="jenkins-username"
                                            variant="outlined"
                                            fullWidth
                                        />
                                    </Grid>

                                    <Grid item xs={12} md={6}>
                                        <FormControl fullWidth>
                                            <TextField
                                                label="API Key"
                                                value={jenkins.apiKey}
                                                onChange={event => setJenkinsData({
                                                    ...jenkins,
                                                    apiKey: event.target.value
                                                })}
                                                type="password"
                                                name="jenkins-api-key"
                                                variant="outlined"
                                                fullWidth
                                            />
                                            {jenkins.url.length === 0 || jenkins.username.length === 0 ? null :
                                                <FormHelperText>
                                                    Configure <a href={`${jenkins.url}/${jenkins.username}/configure`}
                                                                 target="_blank"
                                                                 rel="noopener noreferrer">your Jenkins profile</a>, then click
                                                    "Add new Token"
                                                </FormHelperText>}
                                        </FormControl>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid item xs={12}>
                                <Typography variant="h5">OPC (LED) Settings</Typography>
                            </Grid>

                            <Grid item xs={12}>
                                <Grid container spacing={3}>
                                    <Grid item xs={12} md={8}>
                                        <TextField
                                            name="fadecandy-host"
                                            label="FadeCandy Hostname"
                                            value={fadeCandy.host}
                                            onChange={event => setFadeCandyData({...fadeCandy, host: event.target.value})}
                                            variant="outlined"
                                            fullWidth/>
                                    </Grid>

                                    <Grid item xs={12} md={4}>
                                        <TextField
                                            name="fadecandy-port"
                                            label="Port"
                                            value={fadeCandy.port}
                                            onChange={event => setFadeCandyData({...fadeCandy, port: parseInt(event.target.value)})}
                                            type="number"
                                            variant="outlined"
                                            fullWidth/>
                                    </Grid>

                                    <Grid item xs={12} md={8}>
                                        <StatusDisplayOptions
                                            colours={fadeCandy.statusColours}
                                            animations={fadeCandy.statusAnimations}
                                            onColoursChanged={statusColours => setFadeCandyData({...fadeCandy, statusColours})}
                                            onAnimationsChanged={statusAnimations => setFadeCandyData({...fadeCandy, statusAnimations})}
                                        />
                                    </Grid>

                                    <Grid item xs={12} md={4}>
                                        <Grid container>
                                            <Grid item xs={12}>
                                                <TextField
                                                    name="fadecandy-num-leds"
                                                    label="# of LEDs"
                                                    value={fadeCandy.numLeds}
                                                    onChange={event => setFadeCandyData({...fadeCandy, numLeds: parseInt(event.target.value)})}
                                                    type="number"
                                                    variant="outlined"
                                                    fullWidth/>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <FormControlLabel control={
                                                    <Checkbox
                                                        checked={fadeCandy.useAllLeds}
                                                        onChange={() => setFadeCandyData({...fadeCandy, useAllLeds: !fadeCandy.useAllLeds})} />
                                                } label="Use all LEDs" />
                                            </Grid>
                                            {fadeCandy.useAllLeds ? null :
                                                <Grid item xs={12}>
                                                    <TextField
                                                        name="fadecandy-leds-per-project"
                                                        label="# LEDs per project"
                                                        value={fadeCandy.ledsPerProject}
                                                        onChange={event => setFadeCandyData({...fadeCandy, ledsPerProject: parseInt(event.target.value)})}
                                                        type="number"
                                                        variant="outlined"
                                                        fullWidth />
                                                </Grid>}
                                        </Grid>
                                    </Grid>

                                    <Grid item xs={12}>
                                        <div className={classes.buttons}>
                                            <Button
                                                onClick={() => props.onSubmit({jenkins, fadeCandy, projects: props.settings.projects})}
                                                disabled={fadeCandy === props.settings.fadeCandy && jenkins === props.settings.jenkins}
                                                variant="contained"
                                                color="primary"
                                            >Save</Button>
                                        </div>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid item xs={12} md={4}>

                        <Grid item xs={12}>
                            <Typography variant="h5">Jenkins Projects</Typography>
                        </Grid>

                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                {jenkins.availableProjects.length === 0
                                    ? <NoProjects />
                                    : <ProjectList
                                        jenkins={jenkins}
                                        fadeCandy={fadeCandy}
                                        projectStatuses={props.settings.projects.statuses}
                                        onSelectProject={selectProject}
                                        onUpdateProjectOrder={updateProjectOrder} />}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    );
};

const NoProjects = () =>
    <React.Fragment>
        <Typography variant="h6">No projects found</Typography>
        <Typography variant="body1">
            Either the Jenkins server cannot be reached, or there are no projects configured.
        </Typography>
    </React.Fragment>;

interface IProjectListProps {
    jenkins: IJenkinsSettings;
    fadeCandy: IFadeCandySettings;
    projectStatuses: IProjectStatus[];
    onSelectProject: (project:string, selected: boolean) => void;
    onUpdateProjectOrder: (availableProjects:IJenkinsProject[]) => void;
}

const ProjectList = ({jenkins, fadeCandy, projectStatuses, onSelectProject, onUpdateProjectOrder}:IProjectListProps) => {

    const [projects, setProjects] = useState(jenkins.availableProjects);

    const reorderProject = useCallback(
        (dragIndex: number, hoverIndex: number) => {
            const draggedProject = projects[dragIndex];
            const hoveredProject = projects[hoverIndex];

            const firstSplice  = Math.min(dragIndex, hoverIndex);
            const secondSplice = Math.max(dragIndex, hoverIndex);

            const parts:IJenkinsProject[] = ([] as IJenkinsProject[])
                    .concat(projects.slice(0, firstSplice))
                    .concat(hoverIndex < dragIndex ? draggedProject : hoveredProject)
                    .concat(projects.slice(firstSplice + 1, secondSplice))
                    .concat(hoverIndex < dragIndex ? hoveredProject : draggedProject)
                    .concat(projects.slice(secondSplice + 1));

            setProjects(parts);
        },
        [projects, setProjects],
    );

    return (
        <List>
            {projects.map((project, index) => {
                const projectStatus = projectStatuses.find(p => p.project === project.path);
                const status:JenkinsStatus = projectStatus == null ? 'UNKNOWN' : projectStatus.status;
                const isChecked = jenkins.projects.find(path => path === project.path) != null;
                if (isChecked) {
                    const ledRange = ledRangeForProject(project.path, jenkins.projects, fadeCandy.numLeds, fadeCandy.useAllLeds, fadeCandy.ledsPerProject);
                    return (
                        <ProjectListItem
                            key={project.path}
                            project={project}
                            status={status}
                            statusColour={fadeCandy.statusColours[status]}
                            statusAnimation={fadeCandy.statusAnimations[status]}
                            isChecked={true}
                            ledRange={ledRange}
                            onSelectProject={onSelectProject}
                            jenkinsUrl={jenkins.url}
                            reorderProject={reorderProject}
                            dropProject={() => onUpdateProjectOrder(projects)}
                            index={index}
                        />
                    );
                } else {
                    return (
                        <ProjectListItem
                            key={project.path}
                            project={project}
                            status={status}
                            statusColour={fadeCandy.statusColours[status]}
                            statusAnimation={fadeCandy.statusAnimations[status]}
                            isChecked={false}
                            ledRange={null}
                            onSelectProject={onSelectProject}
                            jenkinsUrl={jenkins.url}
                            reorderProject={reorderProject}
                            dropProject={() => onUpdateProjectOrder(projects)}
                            index={index}
                        />
                    )
                }
            })}
        </List>
    );
};

interface IProjectListItemBaseProps {
    jenkinsUrl: string;
    project: IJenkinsProject;
    onSelectProject: (project: string, selected: boolean) => void;
    index: number;
    status: JenkinsStatus;
    statusColour: RGBValue;
    statusAnimation: StatusAnimation;
    reorderProject: (dragIndex: number, hoverIndex: number) => void;
    dropProject: () => void;
}

interface IProjectListItemCheckedProps extends IProjectListItemBaseProps{
    isChecked: true;
    ledRange: PixelRange;
}

interface IProjectListItemUncheckedProps extends IProjectListItemBaseProps {
    isChecked: false;
    ledRange: null;
}

type IProjectListItemProps = IProjectListItemCheckedProps | IProjectListItemUncheckedProps;

// The drag and drop code is shamelessley lifted from the official examples of react-dnd:
// http://react-dnd.github.io/react-dnd/examples/sortable/simple.

interface DragItem {
    index: number
    id: string
    type: string
}

const ProjectListItem = (props:IProjectListItemProps) => {

    const ref = useRef<HTMLDivElement>(null);

    const [, drop] = useDrop({
        accept: DragableTypes.PROJECT,
        drop() { props.dropProject() },
        hover(item: DragItem, monitor: DropTargetMonitor) {
            if (!ref.current) {
                return
            }

            const dragIndex = item.index;
            const hoverIndex = props.index;

            // Don't replace items with themselves
            if (dragIndex === hoverIndex) {
                return;
            }

            // Determine rectangle on screen
            const hoverBoundingRect = ref.current!.getBoundingClientRect();

            // Get vertical middle
            const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

            // Determine mouse position
            const clientOffset = monitor.getClientOffset();

            // Get pixels to the top
            const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top;

            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%

            // Dragging downwards
            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return;
            }

            // Dragging upwards
            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return;
            }

            // Time to actually perform the action
            props.reorderProject(dragIndex, hoverIndex);

            // Note: we're mutating the monitor item here!
            // Generally it's better to avoid mutations,
            // but it's good here for the sake of performance
            // to avoid expensive index searches.
            item.index = hoverIndex
        },
    });

    const [{ isDragging }, drag] = useDrag({
        item: {
            type: DragableTypes.PROJECT,
            project: props.project,
            index: props.index,
        },
        collect: (monitor: any) => ({
            isDragging: monitor.isDragging(),
        }),
    });

    drag(drop(ref));

    const link = <a href={`${props.jenkinsUrl}${props.project.path}`} target="_blank" rel="noopener noreferrer">{props.project.path}</a>;

    return (
        <div ref={ref}>
            <ListItem style={{opacity: isDragging ? 0.5 : 1.0}}>
                <ListItemIcon>
                    <Checkbox
                        checked={props.isChecked}
                        onChange={() => props.onSelectProject(props.project.path, !props.isChecked)}
                    />
                </ListItemIcon>
                <ListItemText
                    primary={props.project.name}
                    secondary={link}/>
                    <ListItemSecondaryAction>
                        <div style={{cursor: "grab", display: "flex", alignItems: "center"}}>
                            {!props.isChecked ? null : <LedRangeIndicator range={props.ledRange}/>}
                            <LedStrip colours={[maybeAnimateRgb(props.statusColour, props.statusAnimation)]} />
                            <DragIndicatorIcon />
                        </div>
                    </ListItemSecondaryAction>
            </ListItem>
        </div>
    );
};

interface ILedRangeIndicatorProps {
    range: PixelRange;
}

const LedRangeIndicator = ({range}:ILedRangeIndicatorProps) =>
    <span style={{marginRight: '12px'}}>
        LED {range[0]}-{range[1]}
    </span>;

interface IStatusDisplayOptionsProps {
    colours: StatusColourMap;
    animations: StatusAnimationMap;
    onColoursChanged: (colours: StatusColourMap) => void;
    onAnimationsChanged: (animations: StatusAnimationMap) => void;
}

const StatusDisplayOptions = ({colours, animations, onColoursChanged, onAnimationsChanged}: IStatusDisplayOptionsProps) => {

    const setColour = (status: JenkinsStatus, colour: RGBValue) => {
        const newColours = {...colours};
        newColours[status] = colour;
        onColoursChanged(newColours);
    };

    const setAnimation = (status: JenkinsStatus, animation: StatusAnimation) => {
        const newAnimations = {...animations};
        newAnimations[status] = animation;
        onAnimationsChanged(newAnimations);
    };

    return (
        <div>
            {jenkinsStatuses.map(status =>
                <Grid container key={status}>
                    <Grid item xs={12} sm={4}>
                        <Typography>{statusLabel(status)}</Typography>
                    </Grid>
                    <Grid item xs={6} sm={4}>
                        <ColorPicker
                            value={toHex(colours[status])}
                            onChange={(colour: string) => setColour(status, fromHex(colour))}
                        />
                    </Grid>
                    <Grid item xs={6} sm={4}>
                        <Select
                            value={animations[status]}
                            onChange={event => setAnimation(status, event.target.value as StatusAnimation)}
                            fullWidth
                        >
                            <MenuItem value="none">Solid</MenuItem>
                            <MenuItem value="flash-slow">Flash (slow)</MenuItem>
                            <MenuItem value="flash">Flash</MenuItem>
                            <MenuItem value="flash-fast">Flash (fast)</MenuItem>
                        </Select>
                    </Grid>
                </Grid>
            )}
        </div>
    );
};

const mapState = (state: IStoreState): Pick<IProps, keyof IDataProps> => ({
    settings: state.settings
});

const mapDispatch = (dispatch:ThunkDispatch<IStoreState, any, SettingsActionTypes>): Pick<IProps, keyof ICallbackProps> => ({
    onSubmit: (settings: ISettings) => dispatch(saveSettings(settings)),
});

export const Settings = connect(mapState, mapDispatch)(_Settings);