import {Grid, makeStyles, Paper} from "@material-ui/core";
import React from "react";
import {connect} from "react-redux";
import {IStoreState} from "../../data/store";
import {Dispatch} from "redux";
import {ledRangeForProject} from "../../utils";
import {ledWidth} from "../../components/LedStrip";
import {JenkinsStatusChecker} from "../../components/jenkins-status-checker/JenkinsStatusChecker";
import {JenkinsLedStrip} from "../../components/JenkinsLedStrip";
import {projectLabel} from "../settings/Settings";

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(3, 2),
        width: '100%',
        minHeight: 300,
    },
    invisibleLed: {
        backgroundColor: "white"
    },
    ledAndLabelWrapper: {
        position: 'relative',
        height: '95px',
        overflowX: 'scroll',
    },
    projectNameOverlay: {
        paddingTop: '20px',
        paddingLeft: '12px',
        minHeight: '75px',
        borderLeft: 'solid 1px #777',
        display: 'block',
        position: 'absolute',
        top: 0,
        transition: 'background-color 0.3s ease',
        '&:hover': {
            backgroundColor: 'rgba(200,200,255,0.33)',
        }
    },
}));

interface IProps {
    projects: string[];
    numLeds: number;
    useAllLeds: boolean;
    ledsPerProject: number;
    jenkinsUrl: string;
}

const _Dashboard = (props:IProps) => {

    const { projects, numLeds, useAllLeds, ledsPerProject, jenkinsUrl } = props;

    const classes = useStyles();

    return (
        <Grid container spacing={3}>
            <Paper className={classes.paper}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <div className={classes.ledAndLabelWrapper}>
                            <JenkinsStatusChecker />
                            <JenkinsLedStrip/>
                            {projects.map(project => {
                                const ledRange = ledRangeForProject(project, projects, numLeds, useAllLeds, ledsPerProject);
                                const pixelRange = ledRange.map(i => i * ledWidth);
                                return (
                                    <a
                                        key={project}
                                        href={`${jenkinsUrl}/${project}`}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className={classes.projectNameOverlay}
                                        style={{
                                            marginLeft: pixelRange[0],
                                            width: pixelRange[1] - pixelRange[0],
                                        }}>
                                        {projectLabel(project)}
                                    </a>
                                )
                            })}
                        </div>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    );
};

const mapState = (state:IStoreState) => ({
    projects: state.settings.jenkins.projects,
    numLeds: state.settings.fadeCandy.numLeds,
    useAllLeds: state.settings.fadeCandy.useAllLeds,
    ledsPerProject: state.settings.fadeCandy.ledsPerProject,
    jenkinsUrl: state.settings.jenkins.url,
});

const mapDispatch = (dispatch:Dispatch) => ({

});

export const Dashboard = connect(mapState, mapDispatch)(_Dashboard);
