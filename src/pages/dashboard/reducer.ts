import { Reducer } from 'redux'

export const INIT_DASHBOARD = 'INIT_DASHBOARD';

interface IInitDashboardAction {
    type: typeof INIT_DASHBOARD;
}

export type DashboardActionTypes = IInitDashboardAction;

export const initDashboard = ():DashboardActionTypes => ({
    type: INIT_DASHBOARD,
});

export interface IDashboardData {

}

const initialData:IDashboardData = {
    fadeCandyWebsocket: null,
    ledAnimationTimer: null,
};

export const reducer:Reducer<IDashboardData, DashboardActionTypes> = (state = initialData, action:DashboardActionTypes) => {
    switch (action.type) {
        case "INIT_DASHBOARD":
            return state;

        default:
            return state;
    }
};
