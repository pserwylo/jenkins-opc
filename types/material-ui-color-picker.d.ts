declare module 'material-ui-color-picker' {

    import React from 'react'

    interface IProps {
        value: string;
        onChange: (colour:string) => void;
    }

    export interface ColorPicker extends React.Component<IProps> {}

}
