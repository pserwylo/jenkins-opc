# Jenkins Open Pixel Control Dashboard

Show the status of jenkins jobs on LED strips powered by an Open Pixel Control compatible device (e.g. a FadeCandy).

## Building

The app is built using `create-react-app`. Check out the [documentation](https://github.com/facebook/create-react-app)
for information on building and running the app.

In short: `yarn install && yarn start` will install required dependencies, build the project, and serve
it to `http://localhost:3000`. You can also build the project into a static distributable collection
of files using `yarn build`.